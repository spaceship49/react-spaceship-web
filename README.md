# react-spaceship-web

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/react-spaceship-web.svg)](https://www.npmjs.com/package/react-spaceship-web) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-spaceship-web
npm config set legacy-peer-deps true
npx npm-check -u
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'react-spaceship-web'
import 'react-spaceship-web/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

[//]: # (MIT © []&#40;https://github.com/&#41;)
