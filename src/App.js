import React, {useRef} from 'react';
// import ZeroWorkEditor from "./plugins/Editor";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./plugins/editorStyles.css";
import Form2FileUpload from "./Form2FileUpload";

function App() {
	// const uploadButton = React.createRef();
	const uploadButton = useRef(null);

	return (
		<div style={{
			padding: '15rem'
		}}>
			wefewf
			<Form2FileUpload
				ref={uploadButton}
				input_name="document_withholding_tax_receipt_file_upload"
				removeInputName="document_withholding_tax_receipt_file_upload"
				isCanRemove={false}
				items={[]}
				mode={"view"}
				url={"https://dev.b49erp.com/api/file/upload?type=upload"}
				uploadButtonName={"Upload"}
				maxFileUpload={2}
				showImagePreview={{
					width: '300px',
					height: 'auto',
				}}
				// onUploadDone={(file) => {
				// 	console.log(file);
				// 	return true;
				// }}
				onSuccess={(file) => {
					console.log(file);
				}}
				button={(ref) => {
					return (<div
						className="bg-gray-200 w-full aspect-square text-gray-700 text-center rounded flex flex-col justify-center cursor-pointer"
						onClick={() => {
							// console.log(this.uploadButton, this.uploadButton.current.parent, window.$(this.uploadButton.current.parent));
							// window.$(this.uploadButton.current.parent.current).find('input[type="file"]').trigger('click');
						}}
					>
						<div>อัพโหลด</div>
					</div>)
				}}
				data={false}
				render={(ref, data) => {
					return (
						<div
							className="relative bg-gray-200 w-full aspect-square text-gray-700 text-center rounded flex flex-col justify-center overflow-hidden"
							onClick={() => {
								// window.$(ref).find('input[type="file"]').trigger('click');
							}}
						>
							<div className="absolute top-0 right-0 p-2 cursor-pointer"
							     onClick={() => {

							     }}
							>
								<svg xmlns="http://www.w3.org/2000/svg"
								     fill="none"
								     viewBox="0 0 24 24"
								     strokeWidth={2}
								     stroke="currentColor"
								     className="w-6 h-6">
									<path strokeLinecap="round" strokeLinejoin="round"
									      d="M6 18L18 6M6 6l12 12"/>
								</svg>
							</div>

							<img src={data.file_upload.url} alt=""/>
						</div>
					)
				}}
			/>
			{/*<ZeroWorkEditor mode={"create"}*/}
			{/*                uploadUrl={'https://pre-production-lanna-polo.b49consultancy.com/api/file/upload?type=upload'}*/}
			{/*                ref={(ref) => {*/}
			{/*	                console.log(ref.getContent());*/}
			{/*                }}*/}
			{/*	// onSubmit={(contentState) => {*/}
			{/*	//     window.Global.ajax({*/}
			{/*	//         method: 'POST',*/}
			{/*	//         url: 'https://pre-production-lanna-polo.b49consultancy.com/api/v1/page',*/}
			{/*	//         data: {*/}
			{/*	//             page_id: '',*/}
			{/*	//             title: '',*/}
			{/*	//             content_state: JSON.stringify(convertToRaw(contentState)),*/}
			{/*	//         },*/}
			{/*	//         done: function (response) {*/}
			{/*	//             if (!response.success) {*/}
			{/*	//                 return false;*/}
			{/*	//             }*/}
			{/*	//             window.location = '/?page_id=' + response.page.id;*/}
			{/*	//         }*/}
			{/*	//     });*/}
			{/*	// }}*/}
			{/*/>*/}
		</div>
	);
}

export default App;
