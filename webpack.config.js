const path = require('path');
const { EsbuildPlugin } = require('esbuild-loader');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
	module: {
		rules: [
			// {
			// 	test: /\.(js|jsx)$/,
			// 	exclude: /node_modules/,
			// 	use: ['babel-loader'],
			// },
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				loader: "esbuild-loader",
				options: {
					loader: 'jsx',
					target: 'es2015'
				}
			},
			{
				test: /\.css$/i,
				use: [
					'style-loader',
					MiniCssExtractPlugin.loader,
					'css-loader',
					{
						loader: 'esbuild-loader',
						options: {
							minify: true,
						}
					}
				],
			},
			{
				test: /\.(png|svg|jpg|jpeg|gif|ico)$/,
				exclude: /node_modules/,
				use: ['file-loader?name=[name].[ext]'] // ?name=[name].[ext] is only necessary to preserve the original file name
			}
		],
	},
	resolve: {
		extensions: ['*', '.js', '.jsx'],
	},
	mode: 'production',
	// mode: 'development',
	entry: path.resolve(__dirname, './src/index.js'),
	output: {
		path: path.resolve(__dirname, './public'),
		filename: 'bundle.js',
		library: 'Spaceship',
		// libraryTarget : 'global'
		libraryTarget : 'commonjs2'
		// libraryTarget : 'commonjs-static'
	},
	devServer: {
		contentBase: path.resolve(__dirname, './public'),
	},
	plugins: [
		new MiniCssExtractPlugin()
	],
	optimization: {
		minimizer: [
			new EsbuildPlugin({
				target: "es2015",
				css: true
			})
		]
	}
};
